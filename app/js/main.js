$(function () {

    //Разблокируем поле email, если отмечен чекбокс
    $('#check_email').on('change', function () {
        if (this.checked) {
            $('.email').prop({
                'disabled': false,
                'required': true
            });
        } else {
            $('.email').prop({
                'disabled': true,
                'required': false
            }).val('');
        }
    });


    //Отображаем поле для ввода сообщения,
    //если отмечен чекбокс
    $('#check_message').on('change', function () {
        if (this.checked) {
            $('.message').slideDown();
        } else {
            $('.message').slideUp();
        }
    });


    //Считаем итоговую сумму при вводе суммы перевода
    $('.amount').on('input', function () {
        var rate;
        var amount = $('.amount').val();

        //Максимальное колличество символов  для ввода суммы перевода
        if (amount > 500000 && amount.length > 6) {
            $('.amount').val(amount.slice(0,6));
        } else if (amount > 500000 && amount.length > 5) {
            $('.amount').val(amount.slice(0,5));
        }

        //Новое значение amount
        var amountNew = $('.amount').val();

        //Процентная ставка в зависимости от суммы
        if (amountNew < 10000) {
            rate = 1.36
        } else if (amountNew < 50000) {
            rate = 1.20
        } else if (amountNew < 250000) {
            rate = 1.10
        } else {
            rate = 1.0603
        }

        //Выводим итоговую сумму
        if (amount) {
            $('.total_amount').val(Math.round(amountNew * rate));
        } else {
            $('.total_amount').val('');
        }
    });


    //Выводим бренд карточки и CVV-CVC
    $('.card').on('input', '.card_number', function () {
        if ($(this).length < 2) {
            var firstChar = this.value.charAt(0);
            var img;
            var secure;

            switch(firstChar) {
                case '4':
                    img = 'visa.svg';
                    secure = 'CVV';
                    break;
                case '5':
                    img = 'mastercard.svg';
                    secure = 'CVC';
                    break;
                case '6':
                    img = 'maestro.svg';
                    secure = 'CVC';
                    break;
                default:
                    img = '';
                    secure = 'CVV/CVC';
            }

            //Выводим логотип
            if (img) {
                $(this).closest('.card').find('.brand')
                    .html("<img src='./img/" + img + "' alt='" + img.split('.').shift() + "'>");
            } else {
                $(this).closest('.card').find('.brand').html('');
            }

            //Выводим CVV-CVC
            $(this).closest('.card').find('.secure_code').html(secure);
        }
    });


    //Форматирование номера карточки
    $('.card').on('input', '.card_number', function () {
        //Вырезаем нечисловые символы
        var input = $(this).val().replace(/\D/g, '');

        //Обрезаем  до 16 символов
        if (input.length > 16) {
            input = input.slice(0, 16);
        }

        //Делим через пробел по 4 сомвола
        input = input.replace(/(\d{4}(?!$))/g,'$1 ');

        console.log(input);

        //Выводим обратно в input
        $(this).val(input);
    });


    //Форматирование полей месяц и год
    $('.card').on('input', '.month, .year', function () {
        //Вырезаем нечисловые символы
        var input = $(this).val().replace(/\D/g, '');

        //Максимум 2 символа
        if (input.length > 2) {
            input = input.slice(0, 2);
        }

        //Выводим обратно в input
        $(this).val(input);
    });


    //Форматирование CVV/CVC
    $('.card').on('input', '.secure', function () {
        //Вырезаем нечисловые символы
        var input = $(this).val().replace(/\D/g, '');

        //Максимум 4 символа
        if (input.length > 4) {
            input = input.slice(0, 4);
        }

        //Выводим обратно в input
        $(this).val(input);
    });

});